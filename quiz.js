// Copyright (C) 2021 Ivan Efimov, https://gitlab.com/ivan_efimov

const variants_count = 4;
const fetch_timeout = 6000;
const max_log_size = 5;

// Коды завершения для API
const code_ok = 0;
const code_internal_error = 1;
const code_too_many_sessions = 2;
const code_no_such_session = 3;
const code_bad_params = 4;
const code_no_attempts_left = 5;

let variants, chosen_var;
let session_id = "";
let probes_left_text;
let probe_text;
let submit_probe_text;
let probe_result;
let n_in_a_row_label;
let n_in_a_row_counter;

let next_when_answered_overlay;

let session_error_text, session_error_notice;
let retry_notice, retry_button;

let probe_log;

function on_load() {
    init();
    on_next_question();
}

function init() {
    variants = [];
    for (let i = 0; i < variants_count; i++) {
        variants[i] = document.getElementById("var_" + i.toString());
    }
    chosen_var = null;
    probes_left_text = document.getElementById("probes_left");
    probe_text = document.getElementById("probe_text_input");
    probe_text.addEventListener("keydown", on_probe_text_keydown);
    submit_probe_text = document.getElementById("submit_probe_text");
    probe_result = document.getElementById("match_result");
    n_in_a_row_label = document.getElementById("n_in_a_row_label");
    n_in_a_row_counter = 0;

    next_when_answered_overlay = document.getElementById("next_when_answered_overlay");

    session_error_text = document.getElementById("session_error_text");
    session_error_notice = document.getElementById("session_error_notice");
    retry_notice = document.getElementById("retry_notice");
    retry_button = document.getElementById("retry_button");
    probe_log = document.getElementById("probe_log_zone")
}

function reset(new_variants, probes_count) {
    for (let i = 0; i < variants_count; i++) {
        variants[i].classList.remove("var_chosen", "var_correct", "var_incorrect", "var_correct_blinking");
    }
    chosen_var = null;
    probe_text.value = "";
    probe_result.classList.remove("match_ok", "match_failed");
    probe_result.innerText = "Текст ещё не отправлен";
    n_in_a_row_label.innerText = n_in_a_row_counter.toString() + " подряд!";
    if (new_variants) {
        for (let i = 0; i < variants_count; i++) {
            variants[i].innerText = new_variants[i];
        }
    } else {
        for (let i = 0; i < variants_count; i++) {
            variants[i].innerText = "";
        }
    }
    set_probes_left(probes_count);
    hide(session_error_notice);
    hide(retry_notice);
    probe_log.innerHTML = "";
    unlock_interface();
}

function lock_probe_interface() {
    probe_text.disabled = true;
    submit_probe_text.disabled = true;
}

function lock_interface() {
    for (let i = 0; i < variants_count; i++) {
        variants[i].disabled = true;
    }
    lock_probe_interface();
}

function unlock_interface() {
    for (let i = 0; i < variants_count; i++) {
        variants[i].disabled = false;
    }
    probe_text.disabled = false;
    submit_probe_text.disabled = false;
}

function set_probes_left(n) {
    if (n !== null) {
        probes_left_text.innerText = "Осталось проб: " + n.toString();
    } else {
        probes_left_text.innerText = "";
    }
}

function raise_session_error_notice(text) {
    session_error_text.innerText = text;
    show(session_error_notice);
}

function raise_retry_notice(callback) {
    retry_button.onclick = function () {
        callback();
        hide(retry_notice);
    };
    show(retry_notice);
}

function select_variant(i) {
    if (i !== null && i >= 0 && i < variants_count) {
        chosen_var = i;
        variants[chosen_var].classList.add("var_chosen");
    }
    lock_interface();
    check_answer();
}

function append_log(text, ok) {
    let list = probe_log.childNodes;
    if (list.length >= max_log_size) {
        probe_log.removeChild(list[list.length - 1]);
    }
    let label = document.createElement("label");
    label.innerText = text;
    if (ok) {
        label.classList.add("log_ok");
    } else {
        label.classList.add("log_failed");
    }
    probe_log.insertBefore(label, list[0]);
}

function check_answer() {
    let url = "api/validate_answer";
    let params = new URLSearchParams({
        id: session_id,
        variant: chosen_var,
    });
    fetch_with_timeout(url + "?" + params.toString(), fetch_timeout)
        .then((response) => {
            if (response.ok) {
                response.json()
                    .then((data) => {
                        switch (data.code) {
                            case code_ok:
                                apply_result(data.result, data.correct_answer);
                                break;
                            case code_internal_error:
                                raise_session_error_notice("Непредвиденная ошибка на сервере");
                                break;
                            case code_no_such_session:
                                raise_session_error_notice("Сессия не существует либо устарела");
                                break;
                            case code_bad_params:
                                console.log("validate_answer завершился с кодом code_bad_params");
                                raise_session_error_notice("Непредвиденная ошибка на странице");
                                break;
                            default:
                                console.log("validate_answer завершился с неизвестным кодом");
                                raise_session_error_notice("Неизвестная ошибка");
                        }
                    });
            } else {
                raise_retry_notice(check_answer);
            }
        })
        .catch(() => {raise_retry_notice(check_answer)});
}

function apply_result(result, correct_variant) {
    variants[chosen_var].classList.remove("var_chosen");
    if (result) {
        variants[chosen_var].classList.add("var_correct");
        n_in_a_row_counter++;
    } else {
        variants[chosen_var].classList.add("var_incorrect");
        variants[correct_variant].classList.add("var_correct_blinking");
        n_in_a_row_counter = 0;
    }
    show(next_when_answered_overlay);
}

function on_next_question() {
    hide(next_when_answered_overlay);
    fetch_with_timeout("api/next_question", fetch_timeout)
        .then((response) => {
            if (response.ok) {
                response.json()
                    .then((data) => {
                        switch (data.code) {
                            case code_ok:
                                session_id = data.id;
                                reset(data.variants, data.probes_left);
                                break;
                            case code_internal_error:
                                raise_session_error_notice("Непредвиденная ошибка на сервере");
                                break;
                            case code_too_many_sessions:
                                raise_session_error_notice("Сервер перегружен, попробуйте позже");
                                break;
                            default:
                                console.log("validate_answer завершился с неизвестным кодом");
                                raise_session_error_notice("Неизвестная ошибка");
                        }
                    });
            } else {
                raise_retry_notice(on_next_question);
            }
        })
        .catch(() => {raise_retry_notice(on_next_question)});
}

function probe() {
    probe_result.innerText = "Ждём...";
    probe_result.classList.remove("match_ok", "match_failed");
    let url = "api/validate_text"
    let params = new URLSearchParams({
        id: session_id,
        text: probe_text.value,
    });
    fetch_with_timeout(url + "?" + params.toString(), fetch_timeout)
        .then((response) => {
            if (response.ok) {
                response.json()
                    .then((data) => {
                        switch (data.code) {
                            case code_ok:
                                apply_probe_result(data.result, data.probes_left);
                                break;
                            case code_internal_error:
                                raise_session_error_notice("Непредвиденная ошибка на сервере");
                                break;
                            case code_no_such_session:
                                raise_session_error_notice("Сессия не существует либо устарела");
                                break;
                            case code_bad_params:
                                console.log("validate_answer завершился с кодом code_bad_params");
                                raise_session_error_notice("Непредвиденная ошибка на странице");
                                break;
                            case code_no_attempts_left:
                                probe_result.classList.add("match_failed");
                                probe_result.innerText = "Попытки израсходованны!";
                                lock_probe_interface();
                                break;
                            default:
                                console.log("validate_answer завершился с неизвестным кодом");
                                raise_session_error_notice("Неизвестная ошибка");
                        }
                    });
            } else {
                raise_retry_notice(probe);
            }
        })
        .catch(() => {raise_retry_notice(probe)});
}

function apply_probe_result(result, probes_left) {
    if (result) {
        probe_result.classList.add("match_ok");
        probe_result.innerText = "Текст совпадает";
    } else {
        probe_result.classList.add("match_failed");
        probe_result.innerText = "Текст не совпадает";
    }
    set_probes_left(probes_left);
    if (probes_left === 0) {
        lock_probe_interface();
    }
    append_log(probe_text.value, result);
}

// Вспомогательные функции

function show(elem) {
    elem.classList.remove("hidden")
}

function hide(elem) {
    elem.classList.add("hidden")
}

async function fetch_with_timeout(resource, timeout) {
    const abort_controller = new AbortController();
    const id = setTimeout(() => abort_controller.abort(), timeout);

    const response = await fetch(resource, {
        signal: abort_controller.signal
    });
    clearTimeout(id);
    return response;
}

function on_probe_text_keydown(ev) {
    if (ev.key === "Enter") {
        probe();
    }
}
